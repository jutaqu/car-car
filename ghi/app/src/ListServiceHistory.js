import { useEffect, useState } from 'react';

function ServiceHistoryList() {
    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const [vin, setVin] = useState('')
    const [search, setSearch] = useState('')

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    const handleSearch = (event) => {
        const value = event.target.value;
        setVin(value)
    }

    const handleSubmit = () => {
        setSearch(vin);
    }

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/automobiles/');

        if (response.ok) {
            const data = await response.json()
            setAutomobiles(data)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const checkVip = (vin) => {
        return automobiles.some(function(auto) {
            if (auto.vin === vin) {
                return auto.sold
            }
        })
    }

    return (
    <div>
        <div>
            <h1>
                Service History
            </h1>
        </div>
        <div>
        <form id="filter-vin-form">
            <div className="input-group w-auto">
                <input onChange={handleSearch} placeholder="Automobile VIN..." required type="text" name="vin" id="vin" className="form-control"/>
                <button onClick={handleSubmit} className='btn btn-primary' type='button'>Search</button>
            </div>
        </form>
        </div>
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Finish/Cancel</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter(appointment => appointment.vin === search).map(appointment => {
                        return (
                            <tr key={appointment.id}>
                            <td>{ appointment.vin }</td>
                            {(checkVip(appointment.vin)) ? <td>Yes</td>:<td>No</td>}
                            <td>{ appointment.customer }</td>
                            <td>{ new Date(appointment.date_time).toLocaleDateString() }</td>
                            <td>{ new Date(appointment.date_time).toLocaleTimeString() }</td>
                            <td>{ `${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                            <td>{ appointment.reason }</td>
                            <td>{ appointment.status }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    </div>
    )
}

export default ServiceHistoryList;
