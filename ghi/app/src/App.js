import { BrowserRouter, Routes, Route } from 'react-router-dom';
import TechniciansList from './ListTechnicians';
import TechnicianForm from './TechnicianForm';
import AppointmentsList from './ListAppointments';
import AppointmentForm from './AppointmentForm';
import ServiceHistoryList from './ListServiceHistory';
import ManufacturersList from './ListManufacturers';
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './ListModels';
import ModelForm from './ModelForm';
import AutomobilesList from './ListAutomobiles';
import AutomobileForm from './AutomobileForm';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespeopleList from './SalespeopleList';
import CustomersList from './CustomersList';
import SalesList from './SalesList';
import SalespersonForm from './SalespersonForm';
import CustomerForm from './CustomerForm';
import SaleForm from './SaleForm';
import SalespersonHistoryList from './SalespersonHistoryList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="salespeople">
            <Route index element={<SalespeopleList />} />
            <Route path="create" element={<SalespersonForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomersList />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList />} />
            <Route path="create" element={<SaleForm />} />
            <Route path="history" element={<SalespersonHistoryList />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechniciansList />} />
            <Route path="create" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentsList />} />
            <Route path="create" element={<AppointmentForm />} />
            <Route path="history" element={<ServiceHistoryList />} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturersList />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelsList />} />
            <Route path="create" element={<ModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobilesList />} />
            <Route path="create" element={<AutomobileForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
