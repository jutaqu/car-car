from django.urls import path
from .views import (
    api_list_technicians,
    api_detail_technician,
    api_list_appointments,
    api_delete_appointment,
    api_finish_appointment,
    api_cancel_appointment,
    api_detail_autovo,
    api_list_autovos,
    )

urlpatterns = [
    path(
        "technicians/",
        api_list_technicians,
        name="api_list_technicians"
    ),
    path(
        "technicians/<int:pk>/",
        api_detail_technician,
        name="api_detail_technician"
    ),
    path(
        "appointments/",
        api_list_appointments,
        name="api_list_appointments"
    ),
    path(
        "appointments/<int:pk>/",
        api_delete_appointment,
        name="api_delete_appointment"
    ),
    path(
        "appointments/<int:pk>/finish/",
        api_finish_appointment,
        name="api_finish_appointment"
    ),
    path(
        "appointments/<int:pk>/cancel/",
        api_cancel_appointment,
        name="api_cancel_appointment"
    ),
    path(
        "automobiles/<str:vin>/",
        api_detail_autovo,
        name="api_detail_autovo"
    ),
    path("automobiles/",
         api_list_autovos,
         name="api_list_autovos")
]
